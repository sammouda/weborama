# weborama
The project is based on vuejs2 with vuejs-cli template webpack and i modified the config for automatique start navigator when start application ...
I have put a folder with data for developement because the we should have authentification to the api
I have used axios for call web service
I have unit tests in place with mocka and karma and phontomeJs as browser the code is 34% coverage and you find a test/couvrage/lcov-report folder and open index.html.

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload navigator will be open automatically
npm start

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

# To insert component in a site you should include css and js in folder dist to the page and add
<div id="app"></div>

