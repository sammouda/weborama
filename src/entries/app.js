import Vue from 'vue'
import App from '@/App'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)
Vue.config.productionTip = false

/* eslint-disable no-new */
Vue.filter('formatDate', (value) => {
  if (value) {
    let d = new Date(value)
    return d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear()
  }
})

new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})
