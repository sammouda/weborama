import Vue from 'vue'
import GitHubProfile from '@/components/GitHubProfile'
var divTest = document.createElement('div')
divTest.id = 'app'
document.getElementsByTagName('body')[0].appendChild(divTest)
describe('GitHubProfile.vue', () => {
  it('should render correct default id', () => {
    const Constructor = Vue.extend(GitHubProfile)
    const vm = new Constructor().$mount()
    expect(vm.idGit)
    .to.equal('weborama')
  })

  it('should render correct url Api', () => {
    const Constructor = Vue.extend(GitHubProfile)
    const vm = new Constructor().$mount()
    vm.idGit = 'sammouda'
    expect(vm.urlApi)
    .to.equal('https://api.github.com/users/' + vm.idGit)
  })

  it('should call web service with correct ID', (done) => {
    const Constructor = Vue.extend(GitHubProfile)
    const vm = new Constructor().$mount()
    const resolvingPromise = new Promise((resolve) => {
      vm.loadUserData()
    })
    resolvingPromise.then((result) => {
      expect(vm.comp)
      .to.equal(1)
    }).finally(done)
  })
})
